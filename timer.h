#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <time.h>

void timer_reset ( struct timespec *t );
struct timespec timer_start ( void );
struct timespec timer_get ( struct timespec t );
void timer_print ( struct timespec t );
