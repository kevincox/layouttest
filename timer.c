#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <time.h>

#ifndef CLOCK_MONOTONIC_RAW
	#define CLOCK_MONOTONIC_RAW CLOCK_MONOTONIC
#endif

void timer_reset ( struct timespec *t )
{
	clock_gettime(CLOCK_MONOTONIC_RAW, t);
}

struct timespec timer_start ( void )
{
	struct timespec t;
	timer_reset(&t);
	return t;
}

struct timespec timer_get ( struct timespec t )
{
	struct timespec c;
	timer_reset(&c);

	if ( c.tv_nsec < t.tv_nsec )
	{
		c.tv_nsec += 1000000000; // There is room for at least one second.
		c.tv_sec--;
	}

	c.tv_sec  -= t.tv_sec;
	c.tv_nsec -= t.tv_nsec;

	return c;
}

void timer_print ( struct timespec t )
{
	printf("%ld.%09ld seconds\n", t.tv_sec, t.tv_nsec);
}
