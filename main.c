#define _XOPEN_SOURCE 500

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <string.h>
#include <errno.h>
#include <sysexits.h>

#include <ftw.h>
#include <sys/stat.h>
#include <sys/types.h>

//#include "hex.h"
#include "timer.h"

//#define DO_NOTHING

const long unsigned int nsec_per_sec = 1000000000L;

int unlink_cb(const char *fpath, const struct stat *sb, int typeflag,
              struct FTW *ftwbuf)
{
	int rv = remove(fpath);

	if (rv)
	{
		int e = errno;
		fprintf(stderr, "Error: Could not remove file '%s'.\n", fpath);
		fprintf(stderr, "Error: %s\n", strerror(e));
		exit(EX_OSERR);
	}

	return rv;
}
int rmrf(char *path)
{
	return nftw(path, unlink_cb, 64, FTW_DEPTH | FTW_PHYS);
}

void do_xmkdir ( unsigned int mode, char *n )
{
#ifdef DO_NOTHING
	printf("mkdir: %s\n", n);
	return;
#endif
	if ( mkdir(n, 755) == 0 ) return;

	if ( errno == 17 ) // Already exists.
	{
		if ( mode == 1 )
		{
			rmrf(n);
			return do_xmkdir(mode, n);
		}
		else return;
	}

	int e = errno;
	fprintf(stderr, "Error: Could not create directory '%s'.\n", n);
	fprintf(stderr, "Error: %s\n", strerror(e));
	exit(EX_OSERR);
}

void xmkdir ( char *n )
{
	do_xmkdir(2, n);
}

void xmkndir ( char *n )
{
	do_xmkdir(1, n);
}

void writefile ( char *name, char *contents )
{
#ifdef DO_NOTHING
	printf("write: %s\n", name);
	return;
#endif
	FILE *f = fopen(name, "w");
	if (!f)
	{
		int e = errno;
		fprintf(stderr, "Error: Could not write file '%s'.\n", name);
		fprintf(stderr, "Error: %s\n", strerror(e));
		exit(EX_OSERR);
	}

	fputs(contents, f);
	fclose(f);
}

int write_flat ( char *name, char *content )
{
	writefile(name, content);

	return 0;
}

char *read_flat ( char *name, char *buf, size_t size )
{
	FILE *f = fopen(name, "r");
	if (!f)
	{
		int e = errno;
		fprintf(stderr, "Error: Could not open file '%s'.\n", name);
		fprintf(stderr, "Error: %s\n", strerror(e));
		exit(EX_OSERR);
	}

	if (!fgets(buf, size, f))
	{
		int e = errno;
		fprintf(stderr, "Error: Could not read file '%s'.\n", name);
		fprintf(stderr, "Error: %s\n", strerror(e));
		exit(EX_OSERR);
	}

	fclose(f);

	return buf;
}

char *mangle_2char ( char *p, char sep )
{
	unsigned int len = strlen(p);
	if ( len < 3 )
	{
		char *r = p+len-1;
		char *w = p+4;

		*w = '\0'; // Null terminate.
		w--;       //

		*w = *r;   // Write the filename.
		w--; r--;  //

		*w = sep;  // Write the seperator.
		w--;       //

		while ( r >= p ) // Copy the rest of the name.
		{
			*w = *r;
			w--; r--;
		}

		while ( w >= p ) // Pad with 0's.
			*(w--) = '0';
	}
	else
	{
		char *o = p+len+1; // Now pointing at the '\0'+1
		*o = '\0';
		while ( --o > p+2 )
		{
			*o = *(o-1);
		}
		*o = sep;
	}
}

int write_2char ( char *name, char *content )
{
	mangle_2char(name, '\0');
	xmkdir(name);
	name[2] = '/';

	writefile(name, content);

	return 0;
}

char *read_2char ( char *name, char *buf, size_t size )
{
	mangle_2char(name, '/');

	FILE *f = fopen(name, "r");
	if (!f)
	{
		int e = errno;
		fprintf(stderr, "Error: Could not open file '%s'.\n", name);
		fprintf(stderr, "Error: %s\n", strerror(e));
		exit(EX_OSERR);
	}

	if (!fgets(buf, size, f))
	{
		int e = errno;
		fprintf(stderr, "Error: Could not read file '%s'.\n", name);
		fprintf(stderr, "Error: %s\n", strerror(e));
		exit(EX_OSERR);
	}

	fclose(f);

	return buf;
}

char *mangle_3char ( char *p, char sep )
{
	unsigned int len = strlen(p);
	if ( len < 4 )
	{
		char *r = p+len-1;
		char *w = p+5;

		*w = '\0'; // Null terminate.
		w--;       //

		*w = *r;   // Write the filename.
		w--; r--;  //

		*w = sep;  // Write the seperator.
		w--;       //

		while ( r >= p ) // Copy the rest of the name.
		{
			*w = *r;
			w--; r--;
		}

		while ( w >= p ) // Pad with 0's.
			*(w--) = '0';
	}
	else
	{
		char *o = p+len+1; // Now pointing at the '\0'+1
		*o = '\0';
		while ( --o > p+3 )
		{
			*o = *(o-1);
		}
		*o = sep;
	}
}

int write_3char ( char *name, char *content )
{
	mangle_3char(name, '\0');
	xmkdir(name);
	name[3] = '/';

	writefile(name, content);

	return 0;
}

char *read_3char ( char *name, char *buf, size_t size )
{
	mangle_3char(name, '/');

	FILE *f = fopen(name, "r");
	if (!f)
	{
		int e = errno;
		fprintf(stderr, "Error: Could not open file '%s'.\n", name);
		fprintf(stderr, "Error: %s\n", strerror(e));
		exit(EX_OSERR);
	}

	if (!fgets(buf, size, f))
	{
		int e = errno;
		fprintf(stderr, "Error: Could not read file '%s'.\n", name);
		fprintf(stderr, "Error: %s\n", strerror(e));
		exit(EX_OSERR);
	}

	fclose(f);

	return buf;
}

unsigned int mangle_2split ( char *p, char sep )
{
	unsigned int len = strlen(p);
	unsigned int dirs = 0;

	unsigned int offset = len/2;
	unsigned int even = !(len%2);
	if (even) offset++;

	char *r = p+len+1;
	char *w = r+offset;

	offset = 3; // Offset is now the offset from a seperator.
	            // Starting at 3 because we have to copy the null.
	while ( r >= p )
	{
		if (!(offset--))
		{
			*(w--) = sep;
			offset = 1;
			dirs++;
		}

		*(w--) = *(r--);
	}

	while ( w >= p ) // Pad with 0's.
		*(w--) = '0';

	return dirs;
}

int write_2split ( char *name, char *content )
{
	unsigned int dirs = mangle_2split(name, '\0');

	char *e = name-1;
	while ( dirs-- )
	{
		xmkdir(name);
		e += 3;
		*e = '/';
	}

	writefile(name, content);

	return 0;
}

char *read_2split ( char *name, char *buf, size_t size )
{
	mangle_2split(name, '/');

	FILE *f = fopen(name, "r");
	if (!f)
	{
		int e = errno;
		fprintf(stderr, "Error: Could not open file '%s'.\n", name);
		fprintf(stderr, "Error: %s\n", strerror(e));
		exit(EX_OSERR);
	}

	if (!fgets(buf, size, f))
	{
		int e = errno;
		fprintf(stderr, "Error: Could not read file '%s'.\n", name);
		fprintf(stderr, "Error: %s\n", strerror(e));
		exit(EX_OSERR);
	}

	fclose(f);

	return buf;
}

unsigned int mangle_3split ( char *p, char sep )
{
	unsigned int len = strlen(p);
	unsigned int dirs = 0;

	unsigned int offset = len/3; // Number of seperators to add.
	if ( len%3 == 0 ) offset++; // We will zero-pad

	char *r = p+len;
	char *w = r+offset;

	offset = (len%3)+offset; // Offset is now the offset from a seperator.
	                         // The 1 is because we have to copy the null.
	while ( r >= p )
	{
		if ( !(offset--) && r != p )
		{
			*(w--) = sep;
			offset = 2;
			dirs++;
		}

		*(w--) = *(r--);
	}

	while ( w >= p ) // Pad with 0's.
		*(w--) = '0';

	return dirs;
}

int write_3split ( char *name, char *content )
{
	unsigned int dirs = mangle_3split(name, '\0');

	char *e = name-1;
	while ( dirs-- )
	{
		xmkdir(name);
		e += 4;
		*e = '/';
	}

	writefile(name, content);

	return 0;
}

char *read_3split ( char *name, char *buf, size_t size )
{
	mangle_3split(name, '/');

	FILE *f = fopen(name, "r");
	if (!f)
	{
		int e = errno;
		fprintf(stderr, "Error: Could not open file '%s'.\n", name);
		fprintf(stderr, "Error: %s\n", strerror(e));
		exit(EX_OSERR);
	}

	if (!fgets(buf, size, f))
	{
		int e = errno;
		fprintf(stderr, "Error: Could not read file '%s'.\n", name);
		fprintf(stderr, "Error: %s\n", strerror(e));
		exit(EX_OSERR);
	}

	fclose(f);

	return buf;
}

int main ( int argc, char **argv )
{
	unsigned int flags = 0;
	unsigned int max = 10000;
	unsigned int accesses = 1000;

	char *dir = "test/";
	if ( argc > 2 )
	{
		sscanf(argv[1], "%lu-%lu", &max, &accesses);
		dir = argv[2];
	}
	else if ( argc > 1 )
	{
		sscanf(argv[1], "%lu-%lu", &max, &accesses);
	}

	// A buffer that is passed into the functions.  Garenteed to have
	// at least enough space for the longest pathname and 128 extra
	// characters.
	//char *b = malloc((strlen(dir)+strlen(hex[hex_max-1])+130)*sizeof(char));
	char *b = malloc((ceil(log(max)/log(16))+130)*sizeof(char));

	xmkndir(dir);
	chdir(dir);

	struct timespec t;
	double freq;

	size_t read_buf_size = ceil(log(max)/log(16))*sizeof(char);
	char *read_buf = malloc(read_buf_size);

	char **names = malloc(accesses*sizeof(char*));
	if (!names)
	{
		fprintf(stderr, "Error: out of memory");
		exit(EX_UNAVAILABLE);
	}

	srandom(time(NULL));
	unsigned int i;
	for ( i = accesses; i--; )
	{
		unsigned long int r = random()%max;
		char *n = malloc(ceil(log(r)/log(16))*sizeof(char));
		sprintf(n, "%lx", r);

		names[i] = n;
	}

	unsigned int style;
	//style = 5; // For debuging one style.
	for ( style = 1; style <= 5; style++ )
	{
		chdir("..");
		xmkndir(dir);
		chdir(dir);

		int (*write_file)(char*,char*);
		char *(*read_file)(char*,char*, size_t);
		char *style_desc;
		switch (style)
		{
		case 1:
			write_file = &write_flat;
			read_file  = &read_flat;
			style_desc = "flat-file:";
			break;
		case 2:
			write_file = &write_2char;
			read_file  = &read_2char;
			style_desc = "2-char break:";
			break;
		case 3:
			write_file = &write_3char;
			read_file  = &read_3char;
			style_desc = "3-char break:";
			break;
		case 4:
			write_file = &write_2split;
			read_file  = &read_2split;
			style_desc = "2-char splits:";
			break;
		case 5:
			write_file = &write_3split;
			read_file  = &read_3split;
			style_desc = "3-char splits:";
			break;
		default:
			fprintf(stderr, "Error: Unknown style!\n");
			exit(EX_SOFTWARE);
		}

		printf("Creating %-20s ", style_desc);
		fflush(stdout);
		timer_reset(&t);

		unsigned int cur;
		for ( cur = max; cur--; )
		{
			sprintf(b, "%lx", cur);
			write_file(b, b);
		}

		t = timer_get(t);
		timer_print(t);
		freq = max/(double)(t.tv_sec+(t.tv_nsec/(double)nsec_per_sec));
		printf("%lf sec/file, %lf files/sec\n", 1.0/freq, freq);

		printf("Reading %-20s ", style_desc);
		fflush(stdout);
		timer_reset(&t);
		for ( cur = accesses; cur--; )
		{
			strcpy(b, names[cur]);
			read_file(b, read_buf, read_buf_size);
		}

		t = timer_get(t);
		timer_print(t);
		freq = max/(double)(t.tv_sec+(t.tv_nsec/(double)nsec_per_sec));
		printf("%lf sec/file, %lf files/sec\n", 1.0/freq, freq);

		putchar('\n');
	}

	return 0;
}

